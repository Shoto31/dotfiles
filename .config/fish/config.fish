set --export SHELL /usr/bin/fish
set --export EDITOR "subl -w"
set -e fish_greeting


source "$HOME/.config/fish/abbreviations.fish"
